package com.sample.doctorsdemo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PropertiesLogger {

    private static final Logger log = LoggerFactory.getLogger(PropertiesLogger.class);

    @EventListener
    public void handleContextRefreshedEvent(ContextRefreshedEvent event) {
        ConfigurableEnvironment env = (ConfigurableEnvironment) event.getApplicationContext().getEnvironment();

        log.info("Application properties:");
        env.getPropertySources()
                .stream()
                .filter(ps -> ps instanceof MapPropertySource && ps.getName().contains(".properties"))
                .map(ps -> ((MapPropertySource) ps).getSource().keySet())
                .flatMap(java.util.Collection::stream)
                .distinct()
                .sorted()
                .forEach(key -> log.info("{}={}", key, env.getProperty(key)));

        log.info("Environment variables:");
        Map<String, String> envMap = System.getenv();
        for (String envName : envMap.keySet()) {
            log.info(String.format("%s = %s", envName, envMap.get(envName)));
        }
    }
}
