import russianMessages from "@haulmont/ra-language-russian";
import { TranslationMessages } from "ra-core";

export const ru: TranslationMessages = {
  ...russianMessages,
  // place for your massages
};
