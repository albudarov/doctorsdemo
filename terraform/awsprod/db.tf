module "doctorsdemo_db" {
  source = "./db"

  name                                  = var.doctorsdemo_db_name
  engine                                = var.doctorsdemo_db_engine
  engine_version                        = var.doctorsdemo_db_engine_version
  instance_class                        = var.doctorsdemo_db_instance_class
  storage                               = var.doctorsdemo_db_storage
  user                                  = var.doctorsdemo_db_user
  password                              = var.doctorsdemo_db_password
  random_password                       = var.doctorsdemo_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.doctorsdemo_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
  enabled_cloudwatch_logs_exports       = var.doctorsdemo_db_enabled_cloudwatch_logs_exports
  performance_insights_enabled          = var.doctorsdemo_db_performance_insights_enabled
  performance_insights_retention_period = var.doctorsdemo_db_performance_insights_retention_period
  monitoring_interval                   = var.doctorsdemo_db_monitoring_interval
  enhanced_monitoring_arn               = aws_iam_role.rds_enhanced_monitoring.arn
  }

resource "aws_iam_role" "rds_enhanced_monitoring" {
  name_prefix        = "rds-enhanced-monitoring-"
  assume_role_policy = data.aws_iam_policy_document.rds_enhanced_monitoring.json
}

resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring" {
  role       = aws_iam_role.rds_enhanced_monitoring.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

data "aws_iam_policy_document" "rds_enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}
