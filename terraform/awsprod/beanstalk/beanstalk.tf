resource "random_password" "main_db_password" {
  length  = 16
  special = false
}

resource "aws_elastic_beanstalk_application" "app" {
  name        = "${var.name}-${local.prefix}"
  description = "description"
}

resource "aws_iam_role_policy_attachment" "web_tier" {
  count      = var.enable_logging ? 1 : 0
  role       = aws_iam_role.ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "aws_iam_role_policy_attachment" "cloud_watch_agent" {
  count      = var.collect_custom_metrics ? 1 : 0
  role       = aws_iam_role.ec2.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

data "aws_elastic_beanstalk_solution_stack" "docker" {
  most_recent = true

  name_regex = "^64bit Amazon Linux 2 (.*) running Docker(.*)$"
}

resource "aws_elastic_beanstalk_environment" "env" {
  name                = "${var.name}-${local.prefix}-env"
  application         = aws_elastic_beanstalk_application.app.name
  solution_stack_name = data.aws_elastic_beanstalk_solution_stack.docker.name

  cname_prefix        = var.cname_prefix == null ? null : var.cname_prefix

  version_label = aws_elastic_beanstalk_application_version.latest.name

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = var.vpc_id
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = join(",", var.use_private_subnets ? var.private_subnets[*].id : var.public_subnets[*].id)
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = join(",", var.public_subnets[*].id)
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "DBSubnets"
    value     = join(",", var.use_private_subnets ? var.private_subnets[*].id : var.public_subnets[*].id)
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = aws_iam_instance_profile.ec2.name
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = aws_key_pair.key.key_name
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = aws_security_group.instance.id
  }


  setting {
    namespace = "aws:ec2:instances"
    name      = "InstanceTypes"
    value     = var.instance_type
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = !var.use_private_subnets
  }


  setting {
    namespace = "aws:ec2:instances"
    name      = "EnableSpot"
    value     = tostring(var.enable_spot)
  }

  dynamic "setting" {
    for_each = var.enable_spot ? [1] : []
    content {
      namespace = "aws:ec2:instances"
      name      = "SpotMaxPrice"
      value     = var.spot_price
    }
  }

  dynamic "setting" {
    for_each = var.enable_logging ? [1] : []
    content {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "StreamLogs"
      value     = var.enable_logging
    }
  }

  dynamic "setting" {
    for_each = var.delete_logs_on_terminate ? [1] : []
    content {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "DeleteOnTerminate"
      value     = var.delete_logs_on_terminate
    }
  }

  dynamic "setting" {
    for_each = var.enable_logging ? [1] : []
    content {
      namespace = "aws:elasticbeanstalk:cloudwatch:logs"
      name      = "RetentionInDays"
      value     = var.logs_retention
    }
  }

  setting {
    namespace = "aws:elasticbeanstalk:application"
    name      = "Application Healthcheck URL"
    value     = "/"
  }

  dynamic "setting" {
    for_each = var.env
    content {
      namespace = setting.value.namespace
      name      = setting.value.name
      value     = setting.value.value
    }
  }


  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "LoadBalancerType"
    value     = "application"
  }
}

locals {
  ports = join(",\n", [
    for port in var.ports :
    <<-EOF
      EXPOSE ${port}
    EOF
  ])
}

data "archive_file" "latest" {
  type        = "zip"
  output_path = "${var.module_path}/files/${local.tag}.zip"
  source {
    content  = <<-EOF
      FROM ${var.image}
      ${local.ports}
    EOF
    filename = "Dockerfile"
  }
  dynamic "source" {
    for_each = var.collect_custom_metrics ? [1] : []
    content {
      content  = file("${var.module_path}/cloudwatch.config")
      filename = ".ebextensions/cloudwatch.config"
    }
  }
}

resource "aws_s3_bucket" "eb" {
  bucket_prefix = "${var.name}-latest-"
  force_destroy = true
}

resource "aws_s3_object" "docker" {
  bucket = aws_s3_bucket.eb.bucket
  key    = "${local.tag}.zip"
  source = data.archive_file.latest.output_path
}

resource "aws_elastic_beanstalk_application_version" "latest" {
  name        = local.tag
  application = aws_elastic_beanstalk_application.app.name
  bucket      = aws_s3_bucket.eb.bucket
  key         = "${local.tag}.zip"

  depends_on = [aws_s3_object.docker]
}
