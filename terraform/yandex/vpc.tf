module "vpc" {
    source = "./new_vpc"

    cidr_block = var.cidr_block
    name       = var.name
    zone       = var.zone
}
