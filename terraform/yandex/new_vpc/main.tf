resource "yandex_vpc_network" "vpc" {
  name = "${var.name}-vpc"

  labels = {
    name       = "${var.name}-vpc"
    managed-by = "terraform"
  }
}

resource "yandex_vpc_subnet" "subnet" {
  name           = "${var.name}-subnet"
  zone           = var.zone
  network_id     = yandex_vpc_network.vpc.id
  v4_cidr_blocks = [ var.cidr_block ]

  labels = {
    name       = "${var.name}-subnet"
    managed-by = "terraform"
  }
}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = ">= 0.91.0"
    }
  }
  required_version = ">= 0.13"
}
