output "ssh_private_key" {
    value = module.instance.ssh_key
    sensitive = true
}

output "ip" {
    value = module.alb.ip
}