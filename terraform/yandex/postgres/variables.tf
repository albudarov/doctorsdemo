variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "storage" {
  type = number
}

variable "db_instance" {
  type = string
}

variable "zone" {
  type = string
}

variable "username" {
  type = string
}

variable "password" {
  type    = string
  default = null
}

variable "random_password" {
  type    = bool
  default = true
}

variable "performance_diagnostics" {
  type    = bool
  default = false
}

variable "sessions_sampling_interval" {
  type    = number
  default = 70
}

variable "statements_sampling_interval" {
  type    = number
  default = 70
}
